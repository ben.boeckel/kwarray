Function Usefulness
===================

================================================================ ================
 Function name                                                   Usefulness
================================================================ ================
:func:`kwarray.ensure_rng`                                                    179
:func:`kwarray.ArrayAPI`                                                      138
:func:`kwarray.atleast_nd`                                                     36
:func:`kwarray.stats_dict`                                                     30
:func:`kwarray.shuffle`                                                        23
:func:`kwarray.group_indices`                                                  19
:func:`kwarray.one_hot_embedding`                                              17
:func:`kwarray.isect_flags`                                                    15
:func:`kwarray.Stitcher`                                                       14
:func:`kwarray.SlidingWindow`                                                  13
:func:`kwarray.DataFrameArray`                                                 10
:func:`kwarray.embed_slice`                                                     9
:func:`kwarray.normalize`                                                       8
:func:`kwarray.seed_global`                                                     7
:func:`kwarray.padded_slice`                                                    6
:func:`kwarray.RunningStats`                                                    5
:func:`kwarray.boolmask`                                                        5
:func:`kwarray.DataFrameLight`                                                  5
:func:`kwarray.group_consecutive`                                               4
:func:`kwarray.group_items`                                                     4
:func:`kwarray.argmaxima`                                                       3
:func:`kwarray.apply_grouping`                                                  3
:func:`kwarray.mincost_assignment`                                              2
:func:`kwarray.maxvalue_assignment`                                             2
:func:`kwarray.dtype_info`                                                      1
================================================================ ================
